import React, {useState} from 'react';
import {View, StyleSheet, Text, TouchableOpacity} from 'react-native';
import {TextInput as Input, List} from 'react-native-paper';
import {theme} from '../../core/theme';

const DropdownInput = ({
  errorText,
  description,
  input,
  options,
  ...props
}: any) => {
  const [isOpen, setIsOpen] = useState(false);

  const handleToggleDropdown = () => {
    setIsOpen(!isOpen);
  };

  const handleSelectOption = (value: string) => {
    input.onChange(value);

    setIsOpen(false);
  };

  return (
    <View style={styles.container}>
      <View style={styles.dropdownContainer}>
        <Input
          mode="outlined"
          {...props}
          label={props.label}
          value={input.value}
          editable={false}
        />
        <TouchableOpacity
          style={styles.dropdownButton}
          onPress={handleToggleDropdown}
        />
      </View>
      {isOpen && (
        <List.Section style={styles.optionList}>
          {options.map((item: any) => (
            <List.Item
              key={item.value}
              title={item.label}
              onPress={() => handleSelectOption(item.value)}
              style={styles.optionButton}
              titleStyle={styles.optionButtonText}
            />
          ))}
        </List.Section>
      )}
      {description && !errorText ? (
        <Text style={styles.description}>{description}</Text>
      ) : null}
      {errorText ? <Text style={styles.error}>{errorText}</Text> : null}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: '100%',
    marginVertical: 12,
  },
  dropdownContainer: {
    position: 'relative',
  },
  dropdownButton: {
    position: 'absolute',
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    backgroundColor: 'transparent',
  },
  optionList: {
    backgroundColor: '#fff',
    borderRadius: 5,
    elevation: 3,
    marginTop: 10,
  },
  optionButton: {
    borderBottomWidth: 1,
    borderBottomColor: '#e1e1e1',
  },
  optionButtonText: {
    fontSize: 16,
  },
  description: {
    fontSize: 13,
    color: theme.colors.secondary,
    paddingTop: 8,
  },
  error: {
    fontSize: 13,
    color: theme.colors.error,
    paddingTop: 8,
  },
});

export default DropdownInput;
