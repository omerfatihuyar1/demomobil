import {createSlice, PayloadAction} from '@reduxjs/toolkit';
import {Product} from '../../types/types';

interface CardItem extends Product {
  quantity: number;
}

interface CardState {
  items: CardItem[];
}

const initialState: CardState = {
  items: [],
};

const cardSlice = createSlice({
  name: 'card',
  initialState,
  reducers: {
    addToCard: (state, action: PayloadAction<Product>) => {
      const product = action.payload;
      const existingProductIndex = state.items.findIndex(
        item => item.id === product.id,
      );

      if (existingProductIndex !== -1) {
        state.items[existingProductIndex].quantity += 1;
      } else {
        state.items.push({...product, quantity: 1});
      }
    },
    removeFromCard: (state, action: PayloadAction<number>) => {
      const productId = action.payload;
      const existingProductIndex = state.items.findIndex(
        item => item.id === productId,
      );

      if (existingProductIndex !== -1) {
        const product = state.items[existingProductIndex];
        if (product.quantity > 1) {
          product.quantity -= 1;
        } else {
          state.items.splice(existingProductIndex, 1);
        }
      }
    },
    deleteItemFromCard: (state, action: PayloadAction<string>) => {
      const itemId = action.payload;
      state.items = state.items.filter((item: any) => item.id !== itemId);
    },
    increaseQuantity: (state, action: PayloadAction<number>) => {
      const itemId = action.payload;
      const item = state.items.find(item => item.id === itemId);
      if (item) {
        item.quantity += 1;
      }
    },
    decreaseQuantity: (state, action: PayloadAction<number>) => {
      const itemId = action.payload;
      const item = state.items.find(item => item.id === itemId);
      if (item && item.quantity > 1) {
        item.quantity -= 1;
      }
    },
    clearCard: state => {
      state.items = [];
    },
  },
});

export const {
  addToCard,
  removeFromCard,
  clearCard,
  deleteItemFromCard,
  increaseQuantity,
  decreaseQuantity,
} = cardSlice.actions;
export default cardSlice.reducer;
