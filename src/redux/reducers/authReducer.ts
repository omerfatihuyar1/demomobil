import {createSlice} from '@reduxjs/toolkit';
import {loginUser} from '../../service/onLoginService';

const authSlice = createSlice({
  name: 'auth',
  initialState: {loggedIn: false, id: null, role: null},
  reducers: {
    logoutUser: state => {
      state.loggedIn = false;
      state.id = null;
      state.role = null;
    },
    loginedUser: (state, action) => {
      console.log('ax', action.payload);
      state.loggedIn = true;
      state.id = action.payload.userId;
      state.role = action.payload.userRole;
    },
  },
  extraReducers: builder => {
    builder.addCase(loginUser.fulfilled, (state, action) => {
      state.loggedIn = true;
    });
  },
});

export const {logoutUser} = authSlice.actions;
export const {loginedUser} = authSlice.actions;
export default authSlice.reducer;
