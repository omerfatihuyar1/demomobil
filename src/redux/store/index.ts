import {configureStore} from '@reduxjs/toolkit';
import authReducer from '../reducers/authReducer';
import {reducer as formReducer} from 'redux-form';
import cardReducer from '../reducers/cardReducer';

export const store = configureStore({
  reducer: {
    authReducer: authReducer,
    cardReducer: cardReducer,
    form: formReducer,
  },
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
