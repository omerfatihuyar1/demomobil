export enum CardActionTypes {
  addToCard = 'addToCard',
  removeToCard = 'removeToCard',
}
