export interface AuthState {
  loggedIn: boolean;
  username: string | null;
}
export interface User {
  username: string;
  phone: string;
  createDate: string;
  roleName: string;
}
export type Product = {
  quantity: number;
  price: number;
  categoryName: string;
  id: number;
  name: string;
  description: string;
};
export type Category = {
  id: number;
  name: string;
};
export interface Order {
  id: string;
  userId: string;
  productList: Product[];
  quantity: number;
  totalPrice: number;
  orderDate: Date;
  status: string;
}
export interface OrderInput {
  userId: string;
  productList: Product[];
  totalQuantity: number;
  totalPrice: number;
  status: string;
}
