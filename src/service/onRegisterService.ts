import axios from 'axios';
import {createAsyncThunk, createSlice} from '@reduxjs/toolkit';
interface RegisterResponse {
  message: string;
  error: string | null;
}

export const registerUser = createAsyncThunk<
  RegisterResponse,
  {username: string; password: string; phone: string}
>('auth/signup', async (credentials, {rejectWithValue}) => {
  try {
    const response = await axios.post<RegisterResponse>(
      'http://localhost:3000/api/auth/signup',
      credentials,
    );
    return response.data;
  } catch (error: any) {
    if (error.response.status === 401) {
      console.log('HTTP Error:', error.response?.data);
      return rejectWithValue('Register Fail');
    } else if (error.request) {
      console.log('Request Error:', error.request);
      return rejectWithValue('Register Fail');
    } else {
      console.log('Error:', error.message);
      return rejectWithValue('Register Fail');
    }
  }
});
