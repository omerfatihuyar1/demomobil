import axios from 'axios';
import {createAsyncThunk} from '@reduxjs/toolkit';
import {Category, Product} from '../types/types';

export const getAllProducts = createAsyncThunk<
  Product[],
  void,
  {rejectValue: string}
>('products/getAllProducts', async (_, {rejectWithValue}) => {
  try {
    const response = await axios.get<Product[]>(
      'http://localhost:3000/api/products',
    );
    return response.data;
  } catch (error: any) {
    console.log('Error:', error.message);
    return rejectWithValue('Get All Products Fail');
  }
});
export const getAllCategories = createAsyncThunk<
  Category[],
  void,
  {rejectValue: string}
>('categories/getAllCategories', async (_, {rejectWithValue}) => {
  try {
    const response = await axios.get<Category[]>(
      'http://localhost:3000/api/productCategory',
    );
    return response.data;
  } catch (error: any) {
    console.log('Error:', error.message);
    return rejectWithValue('Get All Categories Fail');
  }
});
export const createProduct = createAsyncThunk<
  number,
  {
    name: string;
    description: string;
    price: number;
    categoryId: number;
  }
>('product', async (productData, {rejectWithValue}) => {
  try {
    const response = await axios.post<Product>(
      'http://localhost:3000/api/products',
      productData,
    );
    return response.status;
  } catch (error: any) {
    if (error.response && error.response.status === 401) {
      console.log('HTTP Error:', error.response?.data);
      return rejectWithValue('Create Product Fail');
    } else if (error.request) {
      console.log('Request Error:', error.request);
      return rejectWithValue('Create Product Fail');
    } else {
      console.log('Error:', error.message);
      return rejectWithValue('Create Product Fail');
    }
  }
});
