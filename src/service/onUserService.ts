import axios from 'axios';
import {createAsyncThunk} from '@reduxjs/toolkit';
import {User} from '../types/types';

export const getUserById = createAsyncThunk<User, string>(
  'user/getUserById',
  async (id, {rejectWithValue}) => {
    try {
      const response = await axios.get<User>(
        `http://localhost:3000/api/users/id/${id}`,
      );
      return response.data;
    } catch (error: any) {
      if (error.response?.status === 401) {
        console.log('HTTP Error:', error.response?.data);
        return rejectWithValue('Get User Fail');
      } else if (error.request) {
        console.log('Request Error:', error.request);
        return rejectWithValue('Get User Fail');
      } else {
        console.log('Error:', error.message);
        return rejectWithValue('Get User Fail');
      }
    }
  },
);
