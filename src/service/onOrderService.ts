import axios from 'axios';
import {createAsyncThunk, createSlice} from '@reduxjs/toolkit';
import {Order, OrderInput} from '../types/types';
import {useSelector} from 'react-redux';

export const createOrder = createAsyncThunk<
  Order,
  {
    userId: any;
    quantity: number;
    totalPrice: number;
    productList: any;
    orderDate: Date;
    status: string;
  }
>('order', async (credentials, {rejectWithValue}) => {
  try {
    console.log('cc', credentials.userId);
    const response = await axios.post<Order>(
      'http://localhost:3000/api/order/',
      credentials,
    );
    return response.data;
  } catch (error: any) {
    if (error.response && error.response.status === 401) {
      console.log('HTTP Error:', error.response?.data);
      return rejectWithValue('Login Fail');
    } else if (error.request) {
      console.log('Request Error:', error.request);
      return rejectWithValue('Login Fail');
    } else {
      console.log('Error:', error.message);
      return rejectWithValue('Login Fail');
    }
  }
});
export const getOrder = createAsyncThunk<Order[], {userId: string}>(
  'order/getOrder',
  async ({userId}, {rejectWithValue}) => {
    try {
      console.log('aa', userId);
      if (userId) {
        const response = await axios.get<Order[]>(
          `http://localhost:3000/api/order/${userId}`,
        );
        return response.data;
      } else {
        return rejectWithValue('user no');
      }
    } catch (error: any) {
      if (error.response && error.response.status === 401) {
        console.log('HTTP Error:', error.response?.data);
        return rejectWithValue('Get Order Fail');
      } else if (error.request) {
        console.log('Request Error:', error.request);
        return rejectWithValue('Get Order Fail');
      } else {
        console.log('Error:', error.message);
        return rejectWithValue('Get Order Fail');
      }
    }
  },
);
