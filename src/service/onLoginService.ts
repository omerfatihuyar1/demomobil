import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {createAsyncThunk, createSlice} from '@reduxjs/toolkit';

interface User {
  username: string;
  // Diğer kullanıcı bilgileri
}

interface LoginResponse {
  user: User;
  token: string;
  error: string | null;
}

export const loginUser = createAsyncThunk<
  LoginResponse,
  {username: string; password: string}
>('auth/login', async (credentials, {rejectWithValue}) => {
  try {
    const response = await axios.post<LoginResponse>(
      'http://localhost:3000/api/auth/login',
      credentials,
    );

    const data = {
      username: credentials.username,
      token: response.data.token,
    };
    // Kullanıcı bilgilerini AsyncStorage'a kaydetme
    AsyncStorage.setItem('user', JSON.stringify(data));

    return response.data;
  } catch (error: any) {
    if (error.response && error.response.status === 401) {
      console.log('HTTP Error:', error.response?.data);
      return rejectWithValue('Login Fail');
    } else if (error.request) {
      console.log('Request Error:', error.request);
      return rejectWithValue('Login Fail');
    } else {
      console.log('Error:', error.message);
      return rejectWithValue('Login Fail');
    }
  }
});
