import React from 'react';
import {View, StyleSheet} from 'react-native';
import {DataTable} from 'react-native-paper';
import Background from '../components/Background';
import {Order} from '../types/types';

interface Props {
  order: Order[];
}

const OrderListScreenForm: React.FC<Props> = ({order}) => {
  const [page, setPage] = React.useState<number>(0);
  const [numberOfItemsPerPageList] = React.useState([2, 3, 4]);
  const [itemsPerPage, onItemsPerPageChange] = React.useState(
    numberOfItemsPerPageList[0],
  );

  React.useEffect(() => {
    setPage(0);
  }, [itemsPerPage]);

  return (
    <DataTable>
      <DataTable.Header>
        <DataTable.Title textStyle={{fontWeight: '900'}}>ID</DataTable.Title>
        <DataTable.Title textStyle={{fontWeight: '900'}}>Price</DataTable.Title>
        <DataTable.Title textStyle={{fontWeight: '900'}}>Date</DataTable.Title>
        <DataTable.Title textStyle={{fontWeight: '900'}}>
          Status
        </DataTable.Title>
      </DataTable.Header>
      {order.map((item: any) => (
        <DataTable.Row key={item.id}>
          <DataTable.Cell>{item.id}</DataTable.Cell>
          <DataTable.Cell>{item.totalPrice}</DataTable.Cell>
          <DataTable.Cell>{item.orderDate}</DataTable.Cell>
          <DataTable.Cell>{item.status}</DataTable.Cell>
        </DataTable.Row>
      ))}
    </DataTable>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
  },
});

export default OrderListScreenForm;
