import * as React from 'react';
import Logo from '../components/Logo';
import Header from '../components/Header';
import TextView from '../components/TextView';
import {User} from '../types/types';

export default function ProfileScreenForm({user}: {user: User | undefined}) {
  if (user === undefined) {
    return null;
  }
  const createDate = new Date(user.createDate);

  const formattedDate = `${createDate.getDate()}/${
    createDate.getMonth() + 1
  }/${createDate.getFullYear()}`;
  const formattedTime = `${createDate.getHours()}:${createDate.getMinutes()}`;

  return (
    <>
      <Logo />
      <Header>{user.username} Profile</Header>
      <TextView>Phone: +90 {user.phone} </TextView>
      <TextView>Profile Create Date: {formattedDate} </TextView>
      <TextView>Profile Create Time: {formattedTime} </TextView>
      <TextView>Role Name: {user.roleName} </TextView>
    </>
  );
}
