import React, {useState} from 'react';
import {
  DataTable,
  Button,
  IconButton,
  Portal,
  Dialog,
  Text,
} from 'react-native-paper';
import {Product} from '../types/types';
import Header from '../components/Header';
import {TouchableOpacity, View} from 'react-native';

interface Props {
  goToProducts: (navigation: any) => void;
  createOrder: (values: any) => void;
  deleteItem: (itemId: number) => void;
  increaseQuantity: (itemId: number) => void;
  decreaseQuantity: (itemId: number) => void;
  clearCard: (values: any) => void;
  buyToast: () => void;
  cart: Product[];
}

const CardScreenForm: React.FC<Props> = ({
  cart,
  clearCard,
  goToProducts,
  createOrder,
  deleteItem,
}) => {
  const [page, setPage] = useState(0);
  const itemsPerPage = 10;
  const totalPages = Math.ceil(cart.length / itemsPerPage);

  const [selectedItem, setSelectedItem] = useState<Product | null>(null);
  const [quantityPopupVisible, setQuantityPopupVisible] = useState(false);
  const [quantity, setQuantity] = useState(0);

  const handlePageChange = (pageNumber: number) => {
    setPage(pageNumber);
  };

  const startIndex = page * itemsPerPage;
  const currentItems = cart.slice(startIndex, startIndex + itemsPerPage);

  const totalPrice = currentItems.reduce(
    (total, item) => total + item.price * item.quantity,
    0,
  );

  const openQuantityPopup = (item: Product) => {
    setSelectedItem(item);
    setQuantity(item.quantity);
    setQuantityPopupVisible(true);
  };

  const closeQuantityPopup = () => {
    setQuantityPopupVisible(false);
  };

  const updateQuantity = () => {
    if (selectedItem) {
      selectedItem.quantity = quantity;
      setQuantityPopupVisible(false);
    }
  };

  const handleDeleteItem = (itemId: any) => {
    deleteItem(itemId);
    closeQuantityPopup();
  };

  return (
    <>
      {cart.length === 0 ? (
        <>
          <Header>Cart is Empty</Header>
          <Button mode="contained" onPress={goToProducts}>
            Let's Go!
          </Button>
        </>
      ) : (
        <>
          <DataTable>
            <DataTable.Header>
              <DataTable.Title textStyle={{fontWeight: '900'}}>
                Product
              </DataTable.Title>
              <DataTable.Title textStyle={{fontWeight: '900'}} numeric>
                Price
              </DataTable.Title>
              <DataTable.Title textStyle={{fontWeight: '900'}} numeric>
                Quantity
              </DataTable.Title>
              <DataTable.Title textStyle={{fontWeight: '900'}} numeric>
                Subtotal
              </DataTable.Title>
            </DataTable.Header>

            {currentItems.map(item => (
              <DataTable.Row
                key={item.id}
                onPress={() => openQuantityPopup(item)}>
                <DataTable.Cell>{item.name}</DataTable.Cell>
                <DataTable.Cell numeric>${item.price}</DataTable.Cell>
                <DataTable.Cell numeric>{item.quantity}</DataTable.Cell>
                <DataTable.Cell numeric>
                  ${item.price * item.quantity}
                </DataTable.Cell>
              </DataTable.Row>
            ))}
            <DataTable.Row>
              <DataTable.Cell>Total Price:</DataTable.Cell>
              <DataTable.Cell numeric>${totalPrice}</DataTable.Cell>
            </DataTable.Row>
          </DataTable>
          <Text style={{marginTop: 10, opacity: 0.5, color: '#9c27b0'}}>
            Press to edit products.
          </Text>

          <DataTable.Pagination
            page={page}
            numberOfPages={totalPages}
            onPageChange={handlePageChange}
            label={`${startIndex + 1}-${startIndex + currentItems.length} of ${
              cart.length
            }`}
          />
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Button
              style={{marginRight: 5}}
              mode="contained"
              onPress={() => createOrder(totalPrice)}>
              Buy
            </Button>
            <Button mode="contained" onPress={clearCard}>
              Empty The Card
            </Button>
          </View>

          <Portal>
            <Dialog
              visible={quantityPopupVisible}
              onDismiss={closeQuantityPopup}>
              <Dialog.Title>Product Quantity</Dialog.Title>
              <Dialog.Content>
                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                  <IconButton
                    icon="minus"
                    size={20}
                    onPress={() => setQuantity(quantity - 1)}
                  />
                  <Text>{quantity}</Text>
                  <IconButton
                    icon="plus"
                    onPress={() => setQuantity(quantity + 1)}
                  />
                </View>
              </Dialog.Content>

              <Dialog.Actions>
                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                  <Button onPress={updateQuantity}>Save</Button>
                  <Button onPress={() => handleDeleteItem(selectedItem?.id)}>
                    Delete
                  </Button>
                </View>
              </Dialog.Actions>
            </Dialog>
          </Portal>
        </>
      )}
    </>
  );
};

export default CardScreenForm;
