import * as React from 'react';
import {
  Field,
  InjectedFormProps,
  formValueSelector,
  reduxForm,
} from 'redux-form';
import Button from '../components/Button';
import TextInput from '../components/TextInput';
import Logo from '../components/Logo';
import Header from '../components/Header';
import {ConnectedProps, connect} from 'react-redux';
import DropdownInput from '../components/DropdownInput';
import {Category} from '../types/types';

interface IProps extends ConnectedProps<typeof connector> {
  onCreate: (values: any) => void;
  categories: Category[];
}
const CreateProductScreenForm: React.FC<
  IProps & InjectedFormProps<{}, IProps>
> = ({handleSubmit, onCreate, categories}) => (
  <>
    <Logo />
    <Header>New Product</Header>
    <Field
      name="productName"
      type="text"
      component={TextInput}
      label="Product Name"
    />
    <Field
      name="productDesc"
      type="text"
      component={TextInput}
      label="Description"
    />
    <Field
      name="productPrice"
      type="text"
      component={TextInput}
      label="Price"
    />
    <Field
      name="category"
      component={DropdownInput}
      options={categories}
      label="Select a category"
    />

    <Button mode="contained" style={undefined} onPress={handleSubmit(onCreate)}>
      Create Product
    </Button>
  </>
);

const selector = formValueSelector('createProductScreen');
const mapStateToProps = (state: any) => {
  const productName = selector(state, 'productName');
  const productDesc = selector(state, 'productDesc');
  const price = selector(state, 'price');
  const category = selector(state, 'category');

  return {
    productName,
    productDesc,
    price,
    category,
  };
};
const connector = connect(mapStateToProps);

export default connector(
  reduxForm<{}, IProps>({
    form: 'createProductScreen',
    destroyOnUnmount: true,
    forceUnregisterOnUnmount: true,
  })(CreateProductScreenForm),
);
