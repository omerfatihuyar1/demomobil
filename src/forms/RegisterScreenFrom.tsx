import * as React from 'react';

import {
  Field,
  InjectedFormProps,
  formValueSelector,
  reduxForm,
} from 'redux-form';

import Button from '../components/Button';
import TextInput from '../components/TextInput';
import Logo from '../components/Logo';
import Header from '../components/Header';
import {ConnectedProps, connect} from 'react-redux';

interface IProps extends ConnectedProps<typeof connector> {
  onRegister: (values: any) => void;
}
const RegisterScreenFrom: React.FC<IProps & InjectedFormProps<{}, IProps>> = ({
  handleSubmit,
  onRegister,
}) => (
  <>
    <Logo />
    <Header>Sign In!</Header>
    <Field
      name="userName"
      type="text"
      component={TextInput}
      label="User Name"
    />
    <Field
      name="password"
      type="text"
      component={TextInput}
      label="Password"
      secureTextEntry
    />
    <Field name="phoneNumber" type="text" component={TextInput} label="Phone" />

    <Button
      mode="contained"
      style={undefined}
      onPress={handleSubmit(onRegister)}>
      Register
    </Button>
  </>
);

const selector = formValueSelector('registerScreen');
const mapStateToProps = (state: any) => {
  const userName = selector(state, 'userName');
  const password = selector(state, 'password');
  const phone = selector(state, 'phoneNumber');
  return {
    userName,
    password,
    phone,
  };
};
const connector = connect(mapStateToProps);
export default connector(
  reduxForm<{}, IProps>({
    form: 'registerScreen',
    destroyOnUnmount: false,
    forceUnregisterOnUnmount: true,
  })(RegisterScreenFrom),
);
