import * as React from 'react';

import {
  Field,
  InjectedFormProps,
  formValueSelector,
  reduxForm,
} from 'redux-form';

import Button from '../components/Button';
import TextInput from '../components/TextInput';
import Logo from '../components/Logo';
import Header from '../components/Header';
import {ConnectedProps, connect} from 'react-redux';
import {Text} from 'react-native-paper';

interface IProps extends ConnectedProps<typeof connector> {
  onLogin: (values: any) => void;
  onRegister: (values: any) => void;
}
const LoginScreenForm: React.FC<IProps & InjectedFormProps<{}, IProps>> = ({
  handleSubmit,
  onLogin,
  onRegister,
}) => (
  <>
    <Logo />
    <Header>Welcome!</Header>
    <Field
      name="userName"
      type="text"
      component={TextInput}
      label="User Name"
    />
    <Field
      name="password"
      type="text"
      component={TextInput}
      label="Password"
      secureTextEntry
    />

    <Button mode="contained" style={undefined} onPress={handleSubmit(onLogin)}>
      Sign In
    </Button>
    <Text onPress={handleSubmit(onRegister)}>Click to Register!</Text>
  </>
);

const selector = formValueSelector('loginScreen');
const mapStateToProps = (state: any) => {
  const userName = selector(state, 'userName');
  const password = selector(state, 'password');
  return {
    userName,
    password,
  };
};
const connector = connect(mapStateToProps);
export default connector(
  reduxForm<{}, IProps>({
    form: 'loginScreen',
    destroyOnUnmount: false,
    forceUnregisterOnUnmount: true,
  })(LoginScreenForm),
);
