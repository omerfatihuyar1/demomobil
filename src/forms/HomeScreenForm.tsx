import * as React from 'react';

import Button from '../components/Button';
import Logo from '../components/Logo';
import Header from '../components/Header';
import {useDispatch, useSelector} from 'react-redux';
import Background from '../components/Background';
import TextView from '../components/TextView';
import {logoutUser} from '../redux/reducers/authReducer';
import AsyncStorage from '@react-native-async-storage/async-storage';

export default function Dashboard({navigation}: {navigation: any}) {
  const dispacth = useDispatch();
  const logout = async () => {
    dispacth(logoutUser());
    await AsyncStorage.removeItem('user');
  };
  const navigateScreen = (value: string) => {
    navigation.navigate(value);
  };
  return (
    <Background>
      <Logo />
      <Header>Let's Start!</Header>
      <TextView>
        In this application, you can create and list requests.
      </TextView>
      <Button
        style={undefined}
        mode="outlined"
        onPress={() => navigateScreen('ProductScreen')}>
        Products
      </Button>
      <Button
        style={undefined}
        mode="contained"
        onPress={() => navigateScreen('OrderListScreen')}>
        Order List
      </Button>
      <Button
        style={undefined}
        mode="outlined"
        onPress={() => navigateScreen('ProfileScreen')}>
        Profile
      </Button>
      <Button style={undefined} mode="contained" onPress={() => logout()}>
        Log out
      </Button>
    </Background>
  );
}
