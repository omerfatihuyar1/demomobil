import React, {useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Modal,
  Pressable,
  TouchableOpacity,
} from 'react-native';
import Background from '../components/Background';
import {Category, Product} from '../types/types';
import {DataTable} from 'react-native-paper';
import {useDispatch} from 'react-redux';
import {addToCard} from '../redux/reducers/cardReducer';

interface Props {
  products: Product[];
  categories: Category[];
  user: any;
  successToast: () => void;
  createProducts: () => void;
}

const ProductScreen: React.FC<Props> = ({
  products,
  categories,
  user,
  successToast,
  createProducts,
}) => {
  const [selectedProduct, setSelectedProduct] = useState<Product | null>(null);
  const [modalVisible, setModalVisible] = useState(false);
  const dispatch = useDispatch();
  const [selectedCategory, setSelectedCategory] = useState<string | null>(null);
  const [page, setPage] = useState(0);
  const productsPerPage = 10;

  const handleProductSelect = (product: Product) => {
    setSelectedProduct(product);
    setModalVisible(true);
  };

  const handleAddToCart = (product: Product) => {
    dispatch(addToCard(product));
    successToast();
  };

  const handleCloseModal = () => {
    setSelectedProduct(null);
    setModalVisible(false);
  };

  const handleCategorySelect = (category: string) => {
    setSelectedCategory(category);
    setPage(0);
  };

  const handlePageChange = (page: number) => {
    setPage(page);
  };

  const getProductData = () => {
    if (selectedCategory) {
      const categoryProducts = products.filter(
        product => product.categoryName === selectedCategory,
      );
      const startIndex = page * productsPerPage;
      const endIndex = startIndex + productsPerPage;
      return categoryProducts.slice(startIndex, endIndex);
    } else {
      return [];
    }
  };

  return (
    <>
      <Background>
        <Text style={styles.title}>Categories</Text>
        <View style={styles.categoryList}>
          {categories.map(category => (
            <TouchableOpacity
              key={category.name}
              style={[
                styles.categoryItem,
                selectedCategory === category.name &&
                  styles.selectedCategoryItem,
              ]}
              onPress={() => handleCategorySelect(category.name)}>
              <Text style={styles.categoryItemText}>{category.name}</Text>
            </TouchableOpacity>
          ))}
        </View>

        {selectedCategory && (
          <DataTable>
            <DataTable.Header>
              <DataTable.Title textStyle={{fontWeight: '900'}}>
                Name
              </DataTable.Title>
              <DataTable.Title textStyle={{fontWeight: '900'}} numeric>
                Description
              </DataTable.Title>
              <DataTable.Title textStyle={{fontWeight: '900'}} numeric>
                Price
              </DataTable.Title>
            </DataTable.Header>
            {getProductData().map(product => (
              <DataTable.Row
                key={product.id}
                onPress={() => handleAddToCart(product)}
                onLongPress={() => handleProductSelect(product)}>
                <DataTable.Cell>{product.name}</DataTable.Cell>
                <DataTable.Cell>{product.description}</DataTable.Cell>
                <DataTable.Cell numeric>{product.price}</DataTable.Cell>
              </DataTable.Row>
            ))}
            <Text style={styles.bottomText}>Press to add card.</Text>
            <Text style={styles.bottomText}>Long press details.</Text>

            <DataTable.Pagination
              page={page}
              numberOfPages={Math.ceil(
                getProductData().length / productsPerPage,
              )}
              onPageChange={handlePageChange}
              label={`${page + 1} of ${Math.ceil(
                getProductData().length / productsPerPage,
              )}`}
              style={styles.pagination}
            />
          </DataTable>
        )}
      </Background>
      {user.role === 'Admin' ? (
        <Text onPress={createProducts}>Create new product</Text>
      ) : (
        <></>
      )}
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={handleCloseModal}>
        <View style={styles.modalContainer}>
          <View style={styles.modalContent}>
            <Text style={styles.modalTitle}>{selectedProduct?.name}</Text>
            <Text style={styles.modalDescription}>
              {selectedProduct?.description}
            </Text>
            <Pressable
              style={styles.modalCloseButton}
              onPress={handleCloseModal}>
              <Text style={styles.modalCloseButtonText}>Close</Text>
            </Pressable>
          </View>
        </View>
      </Modal>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    padding: 20,
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 20,
  },
  categoryList: {
    flexDirection: 'row',
    marginBottom: 20,
  },
  categoryItem: {
    flex: 1,
    padding: 10,
    marginHorizontal: 5,
    backgroundColor: '#e1e1e1',
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  selectedCategoryItem: {
    backgroundColor: '#c7c7c7',
  },
  categoryItemText: {
    fontSize: 16,
    fontWeight: 'bold',
  },
  addToCartButton: {
    width: 80,
    alignItems: 'center',
    paddingVertical: 5,
    paddingHorizontal: 10,
    backgroundColor: '#c7c7c7',
    borderRadius: 5,
  },
  addToCartButtonText: {
    fontSize: 14,
    fontWeight: 'bold',
  },
  modalContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  modalContent: {
    backgroundColor: '#fff',
    padding: 20,
    borderRadius: 10,
  },
  modalTitle: {
    fontSize: 18,
    fontWeight: 'bold',
    marginBottom: 10,
  },
  modalDescription: {
    fontSize: 16,
  },
  modalCloseButton: {
    marginTop: 20,
    padding: 10,
    backgroundColor: '#e1e1e1',
    borderRadius: 10,
    alignSelf: 'flex-end',
  },
  modalCloseButtonText: {
    fontSize: 16,
    fontWeight: 'bold',
  },
  pagination: {
    marginTop: 10,
    alignSelf: 'center',
  },
  bottomText: {
    marginTop: 5,
    alignSelf: 'center',
    opacity: 0.6,
    color: '#9c27b0',
  },
});

export default ProductScreen;
