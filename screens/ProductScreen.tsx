import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import BackButton from '../src/components/BackButton';
import ProductScreenForm from '../src/forms/ProductScreenForm';
import {
  getAllCategories,
  getAllProducts,
} from '../src/service/onProductsService';
import {AppDispatch} from '../src/redux/store';
import {Category, Product} from '../src/types/types';
import Toast from 'react-native-toast-message';
import Background from '../src/components/Background';

export default function ProductScreen({navigation}: {navigation: any}) {
  const dispatch: AppDispatch = useDispatch();
  const user = useSelector<any>(state => state.authReducer);
  const [products, setProducts] = useState<Product[]>([]);
  const [categories, setCategories] = useState<Category[]>([]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const productResponse: any = await dispatch(getAllProducts());
        setProducts(productResponse.payload);
        const categoryResponse: any = await dispatch(getAllCategories());
        setCategories(categoryResponse.payload);
      } catch (error) {
        console.log(error);
      }
    };
    fetchData();
  }, []);

  const successToast = () => {
    Toast.show({
      type: 'success',
      text1: 'Product Added',
      text2: 'The product has been successfully added to the cart.',
      position: 'bottom',
    });
  };
  const createProducts = () => {
    navigation.navigate('CreateProduct');
  };

  return (
    <Background>
      <BackButton goBack={navigation.goBack} />
      <ProductScreenForm
        successToast={successToast}
        products={products}
        categories={categories}
        user={user}
        createProducts={createProducts}
      />
      <Toast />
    </Background>
  );
}
