import React from 'react';
import Background from '../src/components/Background';
import BackButton from '../src/components/BackButton';
import {useDispatch, useSelector} from 'react-redux';
import {AppDispatch, RootState} from '../src/redux/store';
import RegisterScreenFrom from '../src/forms/RegisterScreenFrom';
import {registerUser} from '../src/service/onRegisterService';

export default function RegisterScreen({navigation}: {navigation: any}) {
  const dispatch: AppDispatch = useDispatch();
  const authState = useSelector((state: RootState) => state.authReducer);
  const onRegisterPressed = (values: any) => {
    let data = dispatch(
      registerUser({
        username: values.userName,
        password: values.password,
        phone: values.phoneNumber,
      }),
    );
    console.log(data);
  };

  return (
    <Background>
      <BackButton goBack={navigation.goBack} />
      <RegisterScreenFrom onRegister={onRegisterPressed} />
    </Background>
  );
}
