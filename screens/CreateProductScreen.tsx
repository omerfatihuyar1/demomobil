import React, {useEffect, useState} from 'react';
import Background from '../src/components/Background';
import BackButton from '../src/components/BackButton';
import CreateProductScreenForm from '../src/forms/CreateProductScreenForm';
import {AppDispatch} from '../src/redux/store';
import {useDispatch} from 'react-redux';
import {
  createProduct,
  getAllCategories,
} from '../src/service/onProductsService';
import {Category} from '../src/types/types';
export default function CreateProductScreen({navigation}: {navigation: any}) {
  const dispatch: AppDispatch = useDispatch();
  const [categories, setCategories] = useState<Category[]>([]);

  const onCreate = async (values: any) => {
    const response = await dispatch(
      createProduct({
        name: values.productName,
        description: values.productDesc,
        price: values.productPrice,
        categoryId: values.category,
      }),
    );
    const status: any = response.payload;
    status === 201 ? navigation.navigate('HomeScreen') : (values = null);
  };
  useEffect(() => {
    const fetchData = async () => {
      try {
        const categoryResponse: any = await dispatch(getAllCategories());
        setCategories(
          categoryResponse.payload.map((item: any) => ({
            value: item.id,
            label: item.name,
          })),
        );
      } catch (error) {
        console.log(error);
      }
    };
    fetchData();
  }, []);
  return (
    <Background>
      <BackButton goBack={navigation.goBack} />
      <CreateProductScreenForm categories={categories} onCreate={onCreate} />
    </Background>
  );
}
