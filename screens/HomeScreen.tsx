import React, {useState} from 'react';
import Background from '../src/components/Background';
import HomeScreenForm from '../src/forms/HomeScreenForm';
export default function HomeScreen({navigation}: {navigation: any}) {
  return (
    <Background>
      <HomeScreenForm navigation={navigation} />
    </Background>
  );
}
