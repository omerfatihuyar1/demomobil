import React from 'react';
import Background from '../src/components/Background';
import CardScreenForm from '../src/forms/CardScreenForm';
import {useDispatch, useSelector} from 'react-redux';
import {AppDispatch, RootState} from '../src/redux/store';
import BackButton from '../src/components/BackButton';
import {OrderInput} from '../src/types/types';
import {createOrder} from '../src/service/onOrderService';
import {Provider as PaperProvider} from 'react-native-paper';
import {
  clearCard,
  deleteItemFromCard,
  increaseQuantity,
  decreaseQuantity,
} from '../src/redux/reducers/cardReducer';
import Toast from 'react-native-toast-message';

interface Props {
  navigation: any;
}

const CardScreen: React.FC<Props> = ({navigation}) => {
  const cardItems = useSelector((state: RootState) => state.cardReducer.items);
  const user = useSelector((state: RootState) => state.authReducer.id);

  const dispatch: AppDispatch = useDispatch();

  const goToProducts = () => {
    navigation.navigate('ProductScreen');
  };
  const clearCardForm = () => {
    dispatch(clearCard());
    clearCardToast();
  };
  const createOrderButton = async (totalPrice: number) => {
    await dispatch(
      createOrder({
        userId: user,
        quantity: 0,
        totalPrice: totalPrice,
        productList: cardItems,
        orderDate: new Date(),
        status: 'OPEN',
      }),
    );
    clearCardForm();
    buyToast();
  };

  const deleteItemCard = (itemId: any) => {
    dispatch(deleteItemFromCard(itemId));
  };

  const increaseQuantityCard = (itemId: any) => {
    dispatch(increaseQuantity(itemId));
  };

  const decreaseQuantityCard = (itemId: any) => {
    dispatch(decreaseQuantity(itemId));
  };
  const buyToast = () => {
    Toast.show({
      type: 'success',
      text1: 'Order created',
      text2: 'You can follow it from my orders menu.',
      position: 'bottom',
    });
  };
  const clearCardToast = () => {
    Toast.show({
      type: 'error',
      text1: 'Card is Empty',
      position: 'bottom',
    });
  };

  return (
    <PaperProvider>
      <Background>
        <BackButton goBack={navigation.goBack} />
        <CardScreenForm
          cart={cardItems}
          goToProducts={goToProducts}
          createOrder={createOrderButton}
          deleteItem={deleteItemCard}
          increaseQuantity={increaseQuantityCard}
          decreaseQuantity={decreaseQuantityCard}
          clearCard={clearCardForm}
          buyToast={buyToast}
        />
      </Background>
      <Toast />
    </PaperProvider>
  );
};
export default CardScreen;
