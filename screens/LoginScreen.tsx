import React from 'react';
import Background from '../src/components/Background';
import LoginScreenForm from '../src/forms/LoginScreenForm';
import {useDispatch} from 'react-redux';
import {AppDispatch} from '../src/redux/store';
import {loginUser} from '../src/service/onLoginService';

export default function LoginScreen({navigation}: {navigation: any}) {
  const dispatch: AppDispatch = useDispatch();
  const onLoginPressed = async (values: any) => {
    await dispatch(
      loginUser({username: values.userName, password: values.password}),
    );
  };
  const onRegisterPressed = (values: any) => {
    console.log(values);
    navigation.navigate('RegisterScreen');
  };

  return (
    <Background>
      <LoginScreenForm
        onLogin={onLoginPressed}
        onRegister={onRegisterPressed}
      />
    </Background>
  );
}
