import React, {useState, useEffect} from 'react';
import Background from '../src/components/Background';
import {useDispatch, useSelector} from 'react-redux';
import {AppDispatch, RootState} from '../src/redux/store';
import BackButton from '../src/components/BackButton';
import ProfileScreenForm from '../src/forms/ProfileScreenForm';
import {getUserById} from '../src/service/onUserService';
import {User} from '../src/types/types';

export default function ProfileScreen({navigation}: {navigation: any}) {
  const authState: any = useSelector((state: RootState) => state.authReducer);
  const [user, setUser] = useState<User>();

  const dispatch: AppDispatch = useDispatch();

  useEffect(() => {
    fetchUser();
  }, []);

  const fetchUser = async () => {
    try {
      const response: any = await dispatch(getUserById(authState.id));
      setUser(response.payload);
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <Background>
      <BackButton goBack={navigation.goBack} />
      <ProfileScreenForm user={user} />
    </Background>
  );
}
