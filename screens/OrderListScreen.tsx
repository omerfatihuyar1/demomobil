import React, {useEffect, useState} from 'react';
import Background from '../src/components/Background';
import {useDispatch, useSelector} from 'react-redux';
import {AppDispatch} from '../src/redux/store';
import BackButton from '../src/components/BackButton';
import OrderListScreenForm from '../src/forms/OrderListScreenForm';
import {getOrder} from '../src/service/onOrderService';
import {Order} from '../src/types/types';

export default function OrderListScreen({navigation}: {navigation: any}) {
  const dispatch: AppDispatch = useDispatch();
  const user: any = useSelector<any>(state => state.authReducer);
  const [orderResponse, setOrderResponse] = useState<Order[]>([]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response: any = await dispatch(getOrder({userId: user.id}));
        console.log('fatih', response.payload);
        setOrderResponse(response.payload);
      } catch (error) {
        console.log(error);
      }
    };
    fetchData();
  }, []);

  return (
    <Background>
      <BackButton goBack={navigation.goBack} />
      <OrderListScreenForm order={orderResponse} />
    </Background>
  );
}
