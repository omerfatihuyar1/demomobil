import React, {useEffect} from 'react';
import {Provider, useDispatch, useSelector} from 'react-redux';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import LoginScreen from './screens/LoginScreen';
import {AppDispatch, store} from './src/redux/store';
import {NavigationContainer} from '@react-navigation/native';
import HomeScreen from './screens/HomeScreen';
import RegisterScreen from './screens/RegisterScreen';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {loginedUser} from './src/redux/reducers/authReducer';
import ProfileScreen from './screens/ProfileScreen';
import jwtDecode from 'jwt-decode';
import ProductScreen from './screens/ProductScreen';
import 'react-native-vector-icons';
import CartScreen from './screens/CardScreen';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import CreateProductScreen from './screens/CreateProductScreen';
import OrderListScreen from './screens/OrderListScreen';

const Stack = createNativeStackNavigator();
const Tab = createBottomTabNavigator();
const TabNavigator = () => {
  return (
    <Tab.Navigator
      screenOptions={({route}) => ({
        tabBarIcon: ({focused, color, size}) => {
          let iconName;

          if (route.name === 'Home') {
            iconName = focused ? 'home' : 'home-outline';
          } else if (route.name === 'CartScreen') {
            iconName = focused ? 'cart' : 'cart-outline';
          }

          return <Icon name={iconName as string} size={size} color={color} />;
        },
        tabBarActiveTintColor: 'tomato',
        tabBarInactiveTintColor: 'gray',
        headerShown: false,
      })}>
      <Tab.Screen name="Home" component={MyStack} />
      <Tab.Screen name="CartScreen" component={CartScreen} />
    </Tab.Navigator>
  );
};
function MyStack() {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}>
      <Stack.Screen name="HomeScreen" component={HomeScreen} />
      <Stack.Screen name="ProductScreen" component={ProductScreen} />
      <Stack.Screen name="ProfileScreen" component={ProfileScreen} />
      <Stack.Screen name="CreateProduct" component={CreateProductScreen} />
      <Stack.Screen name="OrderListScreen" component={OrderListScreen} />
    </Stack.Navigator>
  );
}
function AuthStack() {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}>
      <Stack.Screen name="LoginScreen" component={LoginScreen} />
      <Stack.Screen name="RegisterScreen" component={RegisterScreen} />
    </Stack.Navigator>
  );
}
function NavigationContainerComp() {
  const dispatch: AppDispatch = useDispatch();
  let user: any = useSelector<any>(state => state.authReducer);

  const getUser = async () => {
    try {
      const loggedInUser: any = await AsyncStorage.getItem('user');
      if (loggedInUser) {
        // Kullanıcı bilgisi varsa oturum açmış kabul ediyoruz
        const data = JSON.parse(loggedInUser);
        const decodedToken: any = jwtDecode(data.token);
        dispatch(loginedUser(decodedToken));
      }
    } catch (error) {
      console.log(error);
    }
  };
  useEffect(() => {
    getUser();
  }, []);
  return (
    <NavigationContainer>
      {user.loggedIn ? <TabNavigator /> : <AuthStack />}
    </NavigationContainer>
  );
}
function App(): JSX.Element {
  return (
    <Provider store={store}>
      <NavigationContainerComp />
    </Provider>
  );
}

export default App;
